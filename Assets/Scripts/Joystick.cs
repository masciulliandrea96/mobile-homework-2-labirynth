using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Joystick : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
{
    public RectTransform joystickBase;
    public RectTransform joystickStick;

    public Vector2 joystickInputVector;
    public bool inuse = false;

    public void OnDrag(PointerEventData eventData)
    {
        CalculateInnerCirclePosition(eventData.position);
        CalculateInputVector();
        CalculateInnerCircleRotation();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        inuse = true;
        OnDrag(eventData);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        inuse = false;
        joystickStick.anchoredPosition = Vector2.zero;
        joystickStick.localRotation = Quaternion.identity;
        joystickInputVector = Vector2.zero;
    }

    private void CalculateInnerCirclePosition(Vector2 position)
    {
        Vector2 directPosition = position - (Vector2)joystickBase.position;
        if (directPosition.magnitude > joystickBase.rect.width / 2f)
            directPosition = directPosition.normalized * joystickBase.rect.width / 2f;
        joystickStick.anchoredPosition = directPosition;
    }

    private void CalculateInputVector()
    {
        joystickInputVector = joystickStick.anchoredPosition / (joystickBase.rect.size / 2f);
        //Debug.Log("JIV: " + joystickInputVector);
    }

    private void CalculateInnerCircleRotation()
    {
        joystickStick.localRotation = Quaternion.Euler(0, 0, Vector2.SignedAngle(Vector2.up, joystickInputVector));
    }
}