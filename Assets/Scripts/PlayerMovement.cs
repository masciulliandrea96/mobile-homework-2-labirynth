using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(PickUpStuff))]
[RequireComponent(typeof(Joystick))]
public class PlayerMovement : MonoBehaviour
{
    public float speed = 400f;
    private Rigidbody rb;
    private Vector3 dir;
    private PickUpStuff pickUpStuff;
    private Vector3 touchedPoint;

    [SerializeField] private Joystick inputSource;


    void Start()
    {
        rb = GetComponent<Rigidbody>();
        pickUpStuff = GetComponent<PickUpStuff>();

    }

    // Update handles input
    private void Update()
    {
        if (inputSource.inuse)
        {
            // if user moves virtual joystick, direction given by joystick input
            dir = new Vector3(-inputSource.joystickInputVector.y, 0, inputSource.joystickInputVector.x);
            if (dir.sqrMagnitude > 0.95) dir.Normalize();
        }
        else if (Input.touchCount > 0)
        {
            // if user touches screen, direction is vector from player to point touched
            Touch touch = Input.GetTouch(0);
            Ray ray = Camera.main.ScreenPointToRay(touch.position);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                touchedPoint = new Vector3(hit.point.x, 0, hit.point.z);
                dir = touchedPoint - transform.position;
                if (dir.sqrMagnitude > 1) dir.Normalize();
            }

        }
        else
        {
            // else direction given by giroscope
            // Remap device acceleration axis to game coordinates
            dir.x = -Input.acceleration.y;
            dir.y = 0;
            dir.z = Input.acceleration.x;
            if (dir.sqrMagnitude > 0.04) dir.Normalize();

        }
        
    }

    // FixedUpdate handles movement
    void FixedUpdate()
    {
        rb.AddForce(dir *  (speed + pickUpStuff.additionalSpeed) * Time.fixedDeltaTime);
    }
}
