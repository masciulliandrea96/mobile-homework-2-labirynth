# [MOBILE - HOMEWORK 2] Labirynth Katamari-Like Mobile Game

Unity project homework made by Andrea Masciulli.

Project designed using IPhone11 resolution (1792x828). 

Other resolutions work fine, but the joystick positioning over the screen may not be as aestetically pleasing.

![mobilelabirinth](https://user-images.githubusercontent.com/32450751/149038141-c12c377b-b084-46f4-9b66-2b57f0722d47.png)
![mobilelabirith2](https://user-images.githubusercontent.com/32450751/149038153-f189ae9f-d0e6-45fa-a16e-a85c3d24abb1.png)
